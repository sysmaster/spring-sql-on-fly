package org.master.sqlonfly.spring.iface;

public interface RowsAffected {
    int value();
}