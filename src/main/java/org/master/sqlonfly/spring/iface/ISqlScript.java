package org.master.sqlonfly.spring.iface;

import java.sql.ResultSet;

public interface ISqlScript {

    <T> T merge(T src, T dest);

    <T> T mergeNotNull(T src, T dest);

    void load(ResultSet rs, Object obj);
}