package org.master.sqlonfly.spring.iface;

import java.sql.ResultSet;

@FunctionalInterface
public interface ObjectTuner<T> {
    void tune(ResultSet rs, T object) throws Exception;
}