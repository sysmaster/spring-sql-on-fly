package org.master.sqlonfly.spring.iface;

public interface ISearchRequest {

    Integer getTotals();

    Integer getItemsPerPage();

    Integer getPage();

    String getOrderBy();

}
