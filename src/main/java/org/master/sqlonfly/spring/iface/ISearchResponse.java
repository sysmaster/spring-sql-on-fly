package org.master.sqlonfly.spring.iface;

import java.util.List;

public interface ISearchResponse<I> {

    public void setItems(List<I> items);

    public void setTotals(Integer value);

    public Class<I> getItemClass();

}
