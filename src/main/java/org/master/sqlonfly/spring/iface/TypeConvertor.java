package org.master.sqlonfly.spring.iface;

@FunctionalInterface
public interface TypeConvertor<F, T> {
    T convert(F source) throws Exception;
}
