package org.master.sqlonfly.spring.iface;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD })
public @interface SQLType {

    /**
     * The mapping column SQL type.
     */
    int value() default java.sql.Types.OTHER;
}
