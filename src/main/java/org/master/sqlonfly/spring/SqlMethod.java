package org.master.sqlonfly.spring;

import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.sql.ResultSet;
import java.util.concurrent.ConcurrentHashMap;

import org.master.sqlonfly.spring.entities.CachedExtractor;
import org.master.sqlonfly.spring.entities.EntityRowMapper;
import org.master.sqlonfly.spring.entities.SqlEntity;
import org.master.sqlonfly.spring.entities.SqlProperty;
import org.master.sqlonfly.spring.entities.Translate;
import org.master.sqlonfly.spring.iface.Dynamic;
import org.master.sqlonfly.spring.iface.ISearchRequest;
import org.master.sqlonfly.spring.iface.ISearchResponse;
import org.master.sqlonfly.spring.iface.ObjectTuner;
import org.master.sqlonfly.spring.iface.TypeConvertor;
import org.springframework.data.jdbc.repository.query.Modifying;
import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.lang.Nullable;

@SuppressWarnings({ "all" })
public class SqlMethod extends ConcurrentHashMap<String, SqlProperty> {
    private static final long serialVersionUID = 1582029285643763309L;

    private Parameter[] params;
    private String name;
    private String template = "";
    private Class<?> resultClass;
    private Class<?> itemClass;
    private Class<?> valueClass;
    private RowMapper<?> mapper;
    private ResultSetExtractor<?> extractor;
    private int callback = -1;
    private int tuner = -1;
    private boolean isUpdate = false;
    private boolean isDynamic = false;
    private boolean isNullable = false;
    private SqlEntity entity;

    public SqlMethod(Method method, ConcurrentHashMap<String, String> statements)
            throws InstantiationException, IllegalAccessException {
        this.name = method.getName();
        Query query = method.getAnnotation(Query.class);
        isUpdate = method.getAnnotation(Modifying.class) != null;
        isDynamic = method.getAnnotation(Dynamic.class) != null;
        isNullable = method.getAnnotation(Nullable.class) != null;

        String script = query != null ? query.value() : "<<" + name + ">>";
        this.template = script.startsWith("<<") ? statements.get(script) : script;

        this.params = method.getParameters();
        for (int i = 0; i < params.length; i++) {
            if (RowCallbackHandler.class.isAssignableFrom(params[i].getType())) {
                this.callback = i;
            } else if (ObjectTuner.class.isAssignableFrom(params[i].getType())) {
                this.tuner = i;
            }
        }

        this.resultClass = method.getReturnType();
        Type itemType = method.getGenericReturnType();

        if (resultClass.isArray()) {
            this.itemClass = resultClass.getComponentType();
        } else if (ISearchResponse.class.isAssignableFrom(resultClass)) {
            ISearchResponse<?> response = (ISearchResponse<?>) resultClass.newInstance();
            this.itemClass = response.getItemClass();
        } else if (itemType instanceof Class<?>) {
            this.itemClass = (Class<?>) itemType;
        } else if (itemType instanceof ParameterizedType) {
            ParameterizedType type = (ParameterizedType) itemType;
            if (type.getActualTypeArguments().length > 0) {
                this.itemClass = (Class<?>) type.getActualTypeArguments()[0];
            }
            if (type.getActualTypeArguments().length > 1) {
                this.valueClass = (Class<?>) type.getActualTypeArguments()[1];
            }
        } else {
            this.itemClass = resultClass;
        }

        entity = SQL.GetEntity(itemClass);

        if (query != null && query.rowMapperClass() != RowMapper.class) {
            this.mapper = query.rowMapperClass().newInstance();
        } else if (query != null && query.resultSetExtractorClass() != ResultSetExtractor.class) {
            this.extractor = query.resultSetExtractorClass().newInstance();
        } else if (ResultSet.class.isAssignableFrom(resultClass)) {
            this.extractor = new CachedExtractor();
        }
    }

    public MapSqlParameterSource getParams(Object[] args) throws Exception {
        MapSqlParameterSource namedParameters = new MapSqlParameterSource();
        for (int i = 0; i < params.length; i++) {
            if (i != callback) {
                Class<?> paramType = params[i].getType();
                String paramName = params[i].getName();

                if (paramType.isEnum()) {
                    namedParameters.addValue(paramName,
                            args[i] == null ? null : SQL.ConvertEnumToValue(paramType, args[i]));

                } else if (paramType.isArray()) {
                    Object[] arr = (Object[]) args[i];
                    if (arr != null) {
                        for (int c = 0; c < arr.length; c++) {
                            Object value = arr[c];
                            if (value != null) {
                                if (value.getClass().isEnum()) {
                                    value = SQL.ConvertEnumToValue(value.getClass(), value);
                                } else {
                                    TypeConvertor convertor = SQL.GetConvertor(value.getClass());
                                    value = convertor == null ? value : convertor.convert(value);
                                }
                            }
                            namedParameters.addValue(paramName + c, value);
                        }
                    }

                } else if (Translate.class.isAssignableFrom(paramType)) {
                    namedParameters.addValue(paramName, args[i] == null ? null : ((Translate) args[i]).serialize());

                } else if (Primitives.is(paramType)) {
                    Object value = args[i];
                    if (value != null) {
                        TypeConvertor convertor = SQL.GetConvertor(value.getClass());
                        value = convertor == null ? value : convertor.convert(value);
                    }
                    namedParameters.addValue(paramName, value);

                } else {
                    SqlEntity entity = SQL.GetEntity(params[i].getType());
                    for (SqlProperty prop : entity.values()) {
                        try {
                            Object value;
                            if (prop.getGetter() != null) {
                                value = prop.getGetter().invoke(args[i]);
                            } else if (prop.getField() != null) {
                                value = prop.getField().get(args[i]);
                            } else {
                                value = null;
                            }

                            if (value != null) {
                                if (value.getClass().isEnum()) {
                                    value = SQL.ConvertEnumToValue(value.getClass(), value);
                                } else {
                                    TypeConvertor convertor = SQL.GetConvertor(value.getClass());
                                    value = convertor == null ? value : convertor.convert(value);
                                }
                            }

                            if (String.class.isAssignableFrom(prop.getJavaType())
                                    && ISearchRequest.class.isAssignableFrom(params[i].getType())) {
                                value = value == null || value.toString().trim().isEmpty() ? "%" : value + "%";
                            }
                            namedParameters.addValue(prop.getName(), value);
                        } catch (Exception e) {
                            throw new RuntimeException("Method param '" + paramName + "' property '" + prop.getName()
                                    + "' append failed to SQL", e);
                        }
                    }

                    if (ISearchRequest.class.isAssignableFrom(params[i].getType())) {
                        ISearchRequest request = (ISearchRequest) args[i];
                        namedParameters.addValue("Offset", (request.getPage() - 1) * request.getItemsPerPage());
                        namedParameters.addValue("Limit", request.getItemsPerPage());
                        namedParameters.addValue("Totals", request.getTotals());
                        namedParameters.addValue("OrderBy", request.getOrderBy());
                    }

                }
            }
        }
        return namedParameters;
    }

    public Class<?> getItemClass() {
        return itemClass;
    }

    public Class<?> getValueClass() {
        return valueClass;
    }

    public RowMapper<?> getMapper(Object[] args) {
        if (this.mapper == null && !Primitives.is(resultClass)) {
            return new EntityRowMapper(itemClass, entity, getTuner(args));
        } else {
            return mapper;
        }
    }

    public RowCallbackHandler getCallback(Object[] args) {
        return callback == -1 ? null : (RowCallbackHandler) args[callback];
    }

    public ObjectTuner<?> getTuner(Object[] args) {
        return tuner == -1 ? null : (ObjectTuner<?>) args[tuner];
    }

    public String getTemplate(Object[] args) throws Exception {
        String result = template;
        for (int i = 0; i < params.length; i++) {
            Class<?> paramType = params[i].getType();
            String paramName = params[i].getName();
            if (paramType.isArray()) {
                Object[] arr = (Object[]) args[i];
                if (arr != null) {
                    StringBuilder buf = new StringBuilder();
                    for (int c = 0; c < arr.length; c++) {
                        if (buf.length() > 0) {
                            buf.append(", ");
                        }
                        buf.append(":").append(paramName).append(c);
                    }
                    result = result.replaceAll(":" + paramName, buf.toString());
                }
            }
        }

        return result;
    }

    public boolean isDynamic() {
        return isDynamic;
    }

    public boolean isNullable() {
        return isNullable;
    }

    public boolean isUpdate() {
        return isUpdate;
    }

    public ResultSetExtractor<?> getExtractor() {
        return extractor;
    }

    public Class<?> getResultClass() {
        return resultClass;
    }

}