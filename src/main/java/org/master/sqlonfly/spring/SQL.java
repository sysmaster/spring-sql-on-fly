package org.master.sqlonfly.spring;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.master.sqlonfly.spring.entities.EntityRowMapper;
import org.master.sqlonfly.spring.entities.NullExtractor;
import org.master.sqlonfly.spring.entities.SqlEntity;
import org.master.sqlonfly.spring.entities.SqlProperty;
import org.master.sqlonfly.spring.iface.ISearchResponse;
import org.master.sqlonfly.spring.iface.ISqlScript;
import org.master.sqlonfly.spring.iface.RowsAffected;
import org.master.sqlonfly.spring.iface.SQLType;
import org.master.sqlonfly.spring.iface.TypeConvertor;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.MappedCollection;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

@SuppressWarnings({ "all" })
public class SQL<T extends ISqlScript> implements InvocationHandler {

    private static final ConcurrentHashMap<Class<?>, EnumMap> ENUM_CACHE = new ConcurrentHashMap<>();
    private static final ConcurrentHashMap<Class<?>, SqlEntity> ENTITY_CACHE = new ConcurrentHashMap<>();

    private static final ConcurrentHashMap<Class<?>, TypeConvertor<?, ?>> TYPE_CONVERTORS = new ConcurrentHashMap<>();
    private static final NullExtractor NULL_EXTRACT = new NullExtractor();

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final ConcurrentHashMap<String, String> statements = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<String, SqlMethod> methods = new ConcurrentHashMap<>();
    private final Class<T> iface;
    private final T script;

    public SQL(Class<T> iface, NamedParameterJdbcTemplate jdbcTemplate) {
        this.iface = iface;
        this.jdbcTemplate = jdbcTemplate;
        this.script = (T) Proxy.newProxyInstance(iface.getClassLoader(), new Class[] { iface }, this);
        loadScripts("\\sql\\" + getClass().getSimpleName() + ".sql");
    }

    public SQL(Class<T> iface, String path, NamedParameterJdbcTemplate jdbcTemplate) {
        this.iface = iface;
        this.jdbcTemplate = jdbcTemplate;
        this.script = (T) Proxy.newProxyInstance(iface.getClassLoader(), new Class[] { iface }, this);
        loadScripts(path);
    }

    private void loadScripts(String path) {
        StringBuilder sb = new StringBuilder();
        try {
            Resource resource = new ClassPathResource(path);
            InputStream input = resource.getInputStream();

            String line = null;
            String statementName = null;

            try (BufferedReader reader = new BufferedReader(new InputStreamReader(input))) {
                while ((line = reader.readLine()) != null) {
                    if (line != null && line.trim().startsWith("<<") && line.trim().endsWith(">>")) {
                        if (statementName != null) {
                            statements.put(statementName, sb.toString());
                        }
                        sb.setLength(0);
                        statementName = line.trim();
                    } else {
                        sb.append(line);
                        sb.append("\n");
                    }
                }
                if (statementName != null) {
                    statements.put(statementName, sb.toString());
                }
            }
        } catch (IOException e) {
            sb.append(e.getMessage());
        }
    }

    public T exec() {
        return script;
    }

    private SqlMethod getMethod(Method method) {
        try {
            SqlMethod result = methods.get(method.getName());
            if (result == null) {
                result = new SqlMethod(method, statements);
                methods.put(method.getName(), result);
            }
            return result;
        } catch (IllegalAccessException | InstantiationException e) {
            throw new RuntimeException(e);
        }
    }

    public static <F, T> void RegisterConvertor(Class<F> from, Class<T> to, TypeConvertor<F, T> converter) {
        TYPE_CONVERTORS.put(from, converter);
    }

    protected Object process(String name, SqlMethod sql, Object[] args) throws Exception {
        MapSqlParameterSource params = sql.getParams(args);
        RowCallbackHandler callback = sql.getCallback(args);

        String template = sql.getTemplate(args);
        if (sql.isDynamic()) {
            StringBuilder buf = new StringBuilder(template);
            for (Map.Entry<String, Object> entry : params.getValues().entrySet()) {
                String key = "${" + entry.getKey() + "}";
                String value = entry.getValue() == null ? "" : entry.getValue().toString();
                int i;
                int l = key.length();
                while ((i = buf.indexOf(key)) != -1) {
                    buf.replace(i, i + l, value);
                }
            }
            template = buf.toString();
        }

        if (sql.isUpdate()) {
            if (KeyHolder.class.isAssignableFrom(sql.getResultClass())) {
                KeyHolder keys = new GeneratedKeyHolder();
                jdbcTemplate.update(template, params, keys);
                return keys;
            } else if (RowsAffected.class.isAssignableFrom(sql.getResultClass())) {
                final int ra = jdbcTemplate.update(template, params);
                return new RowsAffected() {
                    @Override
                    public int value() {
                        return ra;
                    }
                };
            } else {
                jdbcTemplate.update(template, params);
                return null;
            }
        } else {
            RowMapper mapper = sql.getMapper(args);
            if (List.class.isAssignableFrom(sql.getResultClass())
                    || ISearchResponse.class.isAssignableFrom(sql.getResultClass())) {
                if (mapper != null) {
                    if (ISearchResponse.class.isAssignableFrom(sql.getResultClass())) {
                        ISearchResponse response = (ISearchResponse) sql.getResultClass().newInstance();

                        String order = ((String) params.getValue("OrderBy"));
                        if (order != null) {
                            template = template + " ORDER BY " + order;
                        }

                        response.setItems(jdbcTemplate.query(template + " LIMIT :Offset, :Limit", params, mapper));
                        Integer totals = ((Integer) params.getValue("Totals"));
                        if (totals == null || totals == 0) {
                            response.setTotals(jdbcTemplate.queryForObject(
                                    "SELECT COUNT(*) FROM (" + template + ") tmp", params, Integer.class));
                        } else {
                            response.setTotals(totals);
                        }
                        return response;
                    } else {
                        return jdbcTemplate.query(template, params, mapper);
                    }
                } else if (sql.getExtractor() != null) {
                    return jdbcTemplate.query(template, params, sql.getExtractor());
                } else {
                    throw new RuntimeException("No list processor found for " + iface.getName() + "." + name + "()");
                }
            } else if (Map.class.isAssignableFrom(sql.getResultClass())) {
                Map<Object, Object> map = new HashMap<>();
                RowCallbackHandler mapHandler = (rs) -> {
                    Object key = getResultSetObject(sql.getItemClass(), rs, 1);
                    Object value = getResultSetObject(sql.getValueClass(), rs, 2);
                    map.put(key, value);
                };
                jdbcTemplate.query(template, params, mapHandler);
                return map;
            } else if (void.class.isAssignableFrom(sql.getResultClass())) {
                if (callback != null) {
                    jdbcTemplate.query(template, params, callback);
                } else {
                    jdbcTemplate.query(template, params, NULL_EXTRACT);
                }
                return null;
            } else if (ResultSet.class.isAssignableFrom(sql.getResultClass())) {
                if (sql.getExtractor() != null) {
                    return jdbcTemplate.query(template, params, sql.getExtractor());
                } else {
                    throw new RuntimeException("No list resultset found for " + iface.getName() + "." + name + "()");
                }
            } else {
                try {
                    if (mapper != null) {
                        return jdbcTemplate.queryForObject(template, params, mapper);
                    } else {
                        return jdbcTemplate.queryForObject(template, params, sql.getResultClass());
                    }
                } catch (EmptyResultDataAccessException e) {
                    return null;
                }
            }
        }
    }

    protected void loadObject(ResultSet rs, Object obj) throws Exception {
        if (obj != null) {
            SqlEntity entity = this.GetEntity(obj.getClass());
            EntityRowMapper.FillObject(rs, 1, entity, obj);
        }
    }

    public static void Merge(Object src, Object dest) throws Exception {
        Merge(src, dest, true);
    }

    public static void MergeNotNull(Object src, Object dest) throws Exception {
        Merge(src, dest, false);
    }

    private static void Merge(Object src, Object dest, boolean allFields) throws Exception {
        if (src != null && dest != null) {
            SqlEntity entityDest = GetEntity(dest.getClass());
            SqlEntity entitySrc = GetEntity(src.getClass());
            for (SqlProperty destProp : entityDest.values()) {
                Field destFld = destProp.getField();
                Method destSetter = destProp.getSetter();

                SqlProperty srcProp = entitySrc.get(destProp.getName());
                if (srcProp != null) {
                    Field srcFld = srcProp.getField();
                    Method srcGetter = srcProp.getGetter();
                    Object value;

                    if (srcGetter != null) {
                        value = srcGetter.invoke(src);
                    } else if (srcFld != null) {
                        value = srcFld.get(src);
                    } else {
                        value = null;
                    }

                    if (value != null || allFields) {
                        if (destSetter != null) {
                            destSetter.invoke(dest, value);
                        } else if (destFld != null) {
                            destFld.set(dest, value);
                        }
                    }
                }
            }
        }
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if ("merge".equals(method.getName()) || "mergeNotNull".equals(method.getName())) {
            Merge(args[0], args[1], "merge".equals(method.getName()));
            return args[1];

        } else if ("load".equals(method.getName())) {
            loadObject((ResultSet) args[0], args[1]);
            return null;
        } else {
            SqlMethod sql = getMethod(method);
            return process(method.getName(), sql, args);
        }
    }

    public static TypeConvertor<?, ?> GetConvertor(Class<?> clazz) {
        return TYPE_CONVERTORS.get(clazz);
    }

    public static Object ConvertEnumToValue(Class<?> clazz, Object enumItem) {
        if (enumItem == null) {
            return null;
        }

        EnumMap map = ENUM_CACHE.get(clazz);
        if (map == null) {
            map = ScanEnum(clazz);
            ENUM_CACHE.putIfAbsent(clazz, map);
        }

        return map.enumToCode.get(enumItem);
    }

    public static Object ConvertValueToEnum(Class<?> clazz, Object value) {
        if (value == null) {
            return null;
        }

        EnumMap map = ENUM_CACHE.get(clazz);
        if (map == null) {
            map = ScanEnum(clazz);
            ENUM_CACHE.putIfAbsent(clazz, map);
        }

        return map.codeToEnum.get(value);
    }

    private static EnumMap ScanEnum(Class<?> clazz) {
        EnumMap map = new EnumMap();

        Method getter = null;
        for (Method method : clazz.getDeclaredMethods()) {
            if (method.getAnnotation(Id.class) != null) {
                getter = method;
                break;
            }
        }

        try {
            if (getter != null) {
                for (Object item : clazz.getEnumConstants()) {
                    Object code = getter.invoke(item);
                    map.codeToEnum.put(code, item);
                    map.enumToCode.put(item, code);
                }
            } else {
                Object[] items = clazz.getEnumConstants();
                for (int i = 0; i < items.length; i++) {
                    Object item = items[i];
                    Object code = ((Enum) item).name();
                    map.codeToEnum.put(code, item);
                    map.enumToCode.put(item, code);
                }
            }
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            throw new RuntimeException(e);
        }

        return map;
    }

    public static SqlEntity GetEntity(Class<?> clazz) {
        SqlEntity result = ENTITY_CACHE.get(clazz);
        if (result == null) {
            result = ScanEntity(clazz);
            ENTITY_CACHE.put(clazz, result);
        }
        return result;
    }

    private static SqlEntity ScanEntity(Class<?> clazz) {
        SqlEntity entity = new SqlEntity();
        ScanEntity(entity, clazz);
        return entity;
    }

    private static void ScanEntity(SqlEntity entity, Class<?> clazz) {
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            Column column = field.getAnnotation(Column.class);
            SQLType type = field.getAnnotation(SQLType.class);
            if (column != null) {
                field.setAccessible(true);

                String name = column.value() != null && !column.value().trim().isEmpty() ? column.value()
                        : field.getName();

                SqlProperty prop = new SqlProperty();
                entity.put(name, prop);

                prop.setJavaType(field.getType());
                prop.setField(field);
                prop.setType(type == null ? java.sql.Types.OTHER : type.value());
                prop.setName(name);
            }
        }

        Method[] methods = clazz.getDeclaredMethods();
        for (Method method : methods) {
            Column column = method.getAnnotation(Column.class);
            SQLType type = method.getAnnotation(SQLType.class);
            if (column != null) {
                String name = column.value() != null && !column.value().trim().isEmpty() ? column.value()
                        : method.getName().substring(3);

                SqlProperty prop = entity.get(name);
                if (prop == null) {
                    prop = new SqlProperty();
                    prop.setType(type == null ? java.sql.Types.OTHER : type.value());
                    prop.setName(name);
                    entity.put(name, prop);
                }

                if ("get".equals(method.getName().substring(0, 3))) {
                    prop.setGetter(method);
                    prop.setJavaType(method.getReturnType());
                } else if ("set".equals(method.getName().substring(0, 3))) {
                    prop.setJavaType(
                            method.getParameterTypes().length > 0 ? method.getParameterTypes()[0] : String.class);
                    prop.setSetter(method);
                }
            }
        }

        clazz = clazz.getSuperclass();
        if (clazz != null && clazz != Object.class) {
            ScanEntity(entity, clazz);
        }
    }

    public static Object getResultSetObject(Class<?> clazz, ResultSet rs, int index) throws SQLException {
        if (clazz.isEnum()) {
            Object value = rs.getObject(index);
            return value == null ? null : SQL.ConvertValueToEnum(clazz, value);
        } else {
            return rs.getObject(index, clazz);
        }
    }

    private static class EnumMap {
        private ConcurrentHashMap<Object, Object> codeToEnum = new ConcurrentHashMap<>();
        private ConcurrentHashMap<Object, Object> enumToCode = new ConcurrentHashMap<>();
    }
}
