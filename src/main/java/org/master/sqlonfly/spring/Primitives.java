package org.master.sqlonfly.spring;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;

public class Primitives {

    private static ConcurrentHashMap<Class<?>, String> PRIMITIVES;

    private synchronized static ConcurrentHashMap<Class<?>, String> GetMap() {
        if (PRIMITIVES == null) {
            PRIMITIVES = new ConcurrentHashMap<>();
            PRIMITIVES.put(boolean.class, "");
            PRIMITIVES.put(byte.class, "");
            PRIMITIVES.put(char.class, "");
            PRIMITIVES.put(double.class, "");
            PRIMITIVES.put(float.class, "");
            PRIMITIVES.put(int.class, "");
            PRIMITIVES.put(long.class, "");
            PRIMITIVES.put(short.class, "");
            PRIMITIVES.put(Boolean.class, "");
            PRIMITIVES.put(Byte.class, "");
            PRIMITIVES.put(Character.class, "");
            PRIMITIVES.put(Double.class, "");
            PRIMITIVES.put(Float.class, "");
            PRIMITIVES.put(Integer.class, "");
            PRIMITIVES.put(Long.class, "");
            PRIMITIVES.put(Short.class, "");
            PRIMITIVES.put(Date.class, "");
            PRIMITIVES.put(LocalDate.class, "");
            PRIMITIVES.put(LocalTime.class, "");
            PRIMITIVES.put(LocalDateTime.class, "");
            PRIMITIVES.put(String.class, "");
            PRIMITIVES.put(BigDecimal.class, "");
            PRIMITIVES.put(BigInteger.class, "");
        }
        return PRIMITIVES;
    }

    public static boolean is(Class<?> clazz) {
        return GetMap().containsKey(clazz);
    }

}