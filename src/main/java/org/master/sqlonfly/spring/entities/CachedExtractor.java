package org.master.sqlonfly.spring.entities;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.rowset.CachedRowSet;
import javax.sql.rowset.RowSetProvider;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

public class CachedExtractor implements ResultSetExtractor<CachedRowSet> {

    @Override
    public CachedRowSet extractData(ResultSet rs) throws SQLException, DataAccessException {
        CachedRowSet data = RowSetProvider.newFactory().createCachedRowSet();
        data.populate(rs);
        return data;
    }

}
