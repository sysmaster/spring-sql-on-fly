package org.master.sqlonfly.spring.entities;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Translate {

    private final ConcurrentHashMap<String, String> langs = new ConcurrentHashMap<>();

    public String get(String lang) {
        return get(lang, null, null);
    }

    public String get(String lang, String defLang, String defText) {
        String text = langs.get(lang);
        if (text != null) {
            return text;
        }

        if (defLang != null) {
            return langs.get(defLang);
        }

        return defText;
    }

    public void set(String lang, String text) {
        if (lang != null && text != null) {
            langs.put(lang, text);
        }
    }

    public static Translate parse(String value) {
        Translate result = new Translate();
        String[] lines = value.split("\\r?\\n");
        if (lines != null) {
            for (String line : lines) {
                if (line != null) {
                    int split = line.indexOf('=');
                    if (split > 0) {
                        String lang = line.substring(0, split);
                        String text = line.substring(split + 1);
                        result.set(lang, text);
                    }
                }
            }
        }
        return result;
    }

    public String serialize() {
        StringBuilder buf = new StringBuilder();
        for (Map.Entry<String, String> item : langs.entrySet()) {
            buf.append(item.getKey()).append("=").append(item.getValue()).append("\n");
        }
        return buf.toString();
    }
}
