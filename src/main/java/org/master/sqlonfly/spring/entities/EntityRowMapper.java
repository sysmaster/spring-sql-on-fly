package org.master.sqlonfly.spring.entities;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import org.master.sqlonfly.spring.Primitives;
import org.master.sqlonfly.spring.SQL;
import org.master.sqlonfly.spring.iface.ObjectTuner;
import org.springframework.jdbc.core.RowMapper;

@SuppressWarnings({ "all" })
public class EntityRowMapper implements RowMapper<Object> {

    private SqlEntity entity;
    private Class<?> clazz;
    private ObjectTuner tuner;

    public EntityRowMapper(Class<?> clazz, SqlEntity entity, ObjectTuner<?> tuner) {
        this.clazz = clazz;
        this.entity = entity;
        this.tuner = tuner;
    }

    public static void FillObject(ResultSet rs, int rowNum, SqlEntity entity, Object obj) throws Exception {
        for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
            SqlProperty prop = entity.get(rs.getMetaData().getColumnLabel(i));
            if (prop != null) {
                Class<?> clazz = prop.getJavaType();
                if (clazz == Date.class) {
                    clazz = java.sql.Date.class;
                }

                Object value;

                if (clazz == Object.class) {
                    value = rs.getObject(i);

                } else if (clazz.isEnum()) {
                    value = SQL.ConvertValueToEnum(clazz, rs.getObject(i));

                } else if (Translate.class.isAssignableFrom(clazz)) {
                    value = Translate.parse(rs.getString(i));

                } else {
                    if (prop.getType() == java.sql.Types.TIMESTAMP) {
                        value = rs.getTimestamp(i);
                    } else {
                        value = rs.getObject(i, clazz);
                    }
                }

                if (prop.getSetter() != null) {
                    prop.getSetter().invoke(obj, value);
                } else if (prop.getField() != null) {
                    prop.getField().set(obj, value);
                }
            }
        }
    }

    @Override
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
        try {
            if (clazz.isEnum() || Primitives.is(clazz)) {
                return SQL.getResultSetObject(clazz, rs, 1);
            } else {
                Object obj = clazz.newInstance();

                FillObject(rs, rowNum, entity, obj);

                if (tuner != null) {
                    tuner.tune(rs, obj);
                }

                return obj;
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
