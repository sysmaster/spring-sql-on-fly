package org.master.sqlonfly.spring.entities;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

public class NullExtractor implements ResultSetExtractor<Object> {

    @Override
    public Object extractData(ResultSet rs) throws SQLException, DataAccessException {
        return null;
    }

}
